<%@ include file="components/header.jspf"%>
<%@ include file="components/navigation.jspf"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="container">
	<div class="row">
		<div class="col-md-8 mx-auto">
			<form:form method="post" id="upload-form"
				enctype="multipart/form-data" action="/upload">
				<div class="form-group d-flex align-items-center">
					<input class="form-control" required type="file" name="filename"
						id="formFileMultiple" multiple accept=".zip"
						onchange="handleFileSelect(event)" /> <input
						class="btn btn-success btn-sm ml-4" onclick="onClick(event)" name="action" type="submit"
						value="extract" style="margin-left: 10px" /> <input
						class="btn btn-success btn-sm ml-4" onclick="onClick(event)" name="action" type="submit"
						value="get-summarized-text" style="margin-left: 10px" />
				</div>
				<!-- <div class="progress mb-2">
					<div class="progress-bar progress-bar-striped" role="progressbar"
						id="progress-bar" style="width: 0%" aria-valuenow="0"
						aria-valuemin="0" aria-valuemax="100">0%</div>
				</div> -->
			</form:form>
		</div>
	</div>

</div>

<script type="text/javascript">
	var fName;
	function handleFileSelect(event) {
		var fullPath = event.target.value;
		if (fullPath) {
			var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath
					.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
			var filename = fullPath.substring(startIndex);
			if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
				filename = filename.substring(1);
			}
			fName = filename;
			console.log("Full path: " + fullPath); // or do something else with the full path
			console.log("Filename: " + filename); // or do something else with the filename
		}
	}
	function onClick(e)
	{
	const div=document.createElement("div")
	const h1=document.createElement("h1")
	const btn = document.createElement("BUTTON");
	const span=document.createElement("span");
	   div.className="text-center";
	   h1.innerHTML="File is Extracting please wait";
	   btn.className="btn btn-primary";
	   span.className="spinner-border spinner-border-sm";
	   div.appendChild(h1);
	   btn.appendChild(span);
	   div.appendChild(btn);
	   document.body.appendChild(div);
	}
	/* let file=document.getElementById("upload-form") */
	/* const form = document.getElementById('upload-form');
	const fileInput = document.getElementById('formFileMultiple');
	const progressBar = document.querySelector('.progress-bar');

	form.addEventListener('submit', function(e) {
		console.log(e);
		e.preventDefault();
		const file = fileInput.files[0];
		console.log(fName)
		if (file) {
			const xhr = new XMLHttpRequest();
			let url = new URL('http://localhost:8080/upload');
			var x = document.getElementById("formFileMultiple").value;
			const y = document
					.getElementsByClassName("btn btn-success btn-sm ml-4")
			console.log(y[0].value);
			url.searchParams.set('filename', fName);
			url.searchParams.set('action', y[0].value);
			xhr.open('POST', url);
			xhr.upload.addEventListener('progress', function(event) {
				console.log("hello inside form")
				const percent = event.loaded / event.total * 100;
				console.log(percent);
				progressBar.style.width = percent.toFixed(2) + '%';
				progressBar.innerHTML = percent.toFixed(2) + '%';
				progressBar.setAttribute('aria-valuenow', percent.toFixed(2));
			});
			xhr.send(file);
		}
	});
 */	/* function onSubmit(e) {
	   // Set up form and progress bar
	   const form = $('#upload-form')[0];
	   const progressBar = $('#progress-bar');

	   // Submit form on file selection
	   $('#file').change(function() {
	       form.submit();
	   });
	    console.log("hello onSUBMIT")
	   // Handle form submission
	       event.preventDefault();
	       const formData = new FormData($('#upload-form')[0], $('#upload-form')[1]);

	       $.ajax({
	           xhr: function() {
	               // Set up progress bar
	               const xhr = new window.XMLHttpRequest();
	               xhr.upload.addEventListener('progress', function(event) {
	                   if (event.lengthComputable) {
	                       const percentComplete = (event.loaded / event.total) * 100;
	                       console.log(percentComplete);
	                       progressBar.css('width', percentComplete + '%').attr('aria-valuenow', percentComplete);
	                   }
	               });
	               return xhr;
	           },
	           url: '/upload',
	           type: 'POST',
	           data: formData,
	           processData: false,
	           contentType: 'multipart/form-data',
	           });
	}; 
	 *//* document.getElementById('uploadForm').addEventListener('submit', function(e) {
				  e.preventDefault();
				  var formData = new FormData(document.getElementById('uploadForm'));

				  var xhr = new XMLHttpRequest();
				  xhr.open('POST', '/upload', true);

				  xhr.upload.addEventListener('progress', function(e) {
				    var progress = Math.round((e.loaded / e.total) * 100);
				    document.getElementById('progressBar').value = progress;
				    document.getElementById('uploadProgress').value = progress;
				  });

				  xhr.onreadystatechange = function() {
				    if (xhr.readyState == 4 && xhr.status == 200) {
				    	var response = JSON.parse(xhr.responseText);
				        if (response.status == 'success') {
				          document.getElementById('progress-bar').value = response.progress;
				        } else {
				          // Handle errors
				        }
				    }
				  };

				  xhr.send(formData);
				}); */
</script>
<script type="text/javascript" src="webjars/jquery/3.6.0/jquery.min.js"></script>
<%@ include file="components/footer.jspf"%>
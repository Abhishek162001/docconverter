<%@ include file="components/header.jspf" %>
<%@ include file="components/navigation.jspf" %>

<div class="container">
<table class="table" id="table1">
    <thead>
    <tr>
        <th>Column A</th><th>Column B</th><th>Column C</th>
    </tr>
    </thead>
    <tbody>
	    <tr data-node="treetable-01" data-pnode="">
	        <td>Node<span class="treetable-expander fa fa-angle-down"></span></td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to text"/></td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to summarized text"/></td>
	       
	    </tr>
	    <tr data-node="treetable-01.01" data-pnode="treetable-parent-01">
	        <td>Node</td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to text"/></td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to summarized text"/></td>
	      
	    </tr>
	    <tr data-node="treetable-01.01.02" data-pnode="treetable-parent-01.01">
	        <td>Node</td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to text"/></td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to summarized text"/></td>
	       
	    </tr>
	    <tr data-node="treetable-01.01.03" data-pnode="treetable-parent-01.01">
	        <td>Node</td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to text"/></td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to summarized text"/></td>
	        
	    </tr>
	    <tr data-node="treetable-01.01.04" data-pnode="treetable-parent-01.01">
	        <td>Node</td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to text"/></td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to summarized text"/></td>
	        
	    </tr>
	    <tr data-node="treetable-01.02" data-pnode="treetable-parent-01">
	        <td>Node</td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to text"/></td>
	        
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to summarized text"/></td>
	      
	    </tr>
	    <tr data-node="treetable-01.02.01" data-pnode="treetable-parent-01.02">
	        <td>Node</td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to text"/></td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to summarized text"/></td>
	        
	    </tr>
	    <tr data-node="treetable-02">
	        <td>Node</td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to text"/></td>
	        <td><input class="btn btn-success btn-sm ml-4" type="submit" value="convert to  summarized text"/></td>
	        
	    </tr>
	</tbody>
</table>


</div>

<script>
    $(function() {
        $("#table1").treeFy({
            treeColumn: 1
        });
    });
</script>
<%@ include file="components/footer.jspf" %>
package com.anakage.fileUploader.fileUploadService;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface userRepository extends CrudRepository<userInfo,Long>{

	public List<userInfo> findAll(); 
	public Optional<userInfo> findById(Long id);
}

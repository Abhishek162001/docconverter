package com.anakage.fileUploader.fileUploadService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.net.URLConnection;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xslf.extractor.XSLFExtractor;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class FileUploadService {

	int c=0;
	fileRepository fileRepo;
	public FileUploadService(fileRepository fileRepo) {
		super();
		this.fileRepo = fileRepo;
	}

	public void handleFileUpload(@RequestParam("file") MultipartFile file, String filePath, boolean summarizationFlag)
			throws IOException {
		String fileZip = filePath;

		File destDir = new File("D:\\AnakageDocs");

		byte[] buffer = new byte[1024];

		ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));

		ZipEntry zipEntry = zis.getNextEntry();

		while (zipEntry != null) {

			File newFile = newFile(destDir, zipEntry);
			String fileType = URLConnection.guessContentTypeFromName(newFile.getName());
			String fileName = getFilename(newFile, summarizationFlag);
			String absolutefilePath = newFile.getParent() + "\\";
			if (zipEntry.isDirectory()) {
				if (!newFile.isDirectory() && !newFile.mkdirs()) {
					throw new IOException("Failed to create directory " + newFile);
				}
			} else {
				// fix for Windows-created archives
				File parent = newFile.getParentFile();
				if (!parent.isDirectory() && !parent.mkdirs()) {
					throw new IOException("Failed to create directory " + parent);
				}

				// write file content

				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				if (fileType
						.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
					docxToTextConvert(absolutefilePath + getFilename(newFile, false),
							absolutefilePath + getFilename(newFile, summarizationFlag), summarizationFlag,fileName);
					removeFile(newFile);
				} else if (fileType.equalsIgnoreCase("application/pdf")) {
					pdfToTextConverter(absolutefilePath + getFilename(newFile, false),
							absolutefilePath + getFilename(newFile, summarizationFlag), summarizationFlag,fileName);
					removeFile(newFile);
				} else if (fileType.equalsIgnoreCase(
						"application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
					pptxToTextConvert(absolutefilePath + getFilename(newFile, false),
							absolutefilePath + getFilename(newFile, summarizationFlag), summarizationFlag,fileName);
					removeFile(newFile);
				} else if (fileType
						.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
					excelToTextConverter(absolutefilePath + getFilename(newFile, false),
							absolutefilePath + getFilename(newFile, summarizationFlag), summarizationFlag,fileName);
					removeFile(newFile);
				}

			}
			zipEntry = zis.getNextEntry();

		}
		zis.closeEntry();
		zis.close();

	}

	private String getFilename(File newFile, boolean summarizationFlag) {
		String fileName = "";
		if (summarizationFlag) {
			fileName = "summarized_" + newFile.getName();
		} else {
			fileName = newFile.getName();
		}
		return fileName;
	}

	public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
		File destFile = new File(destinationDir, zipEntry.getName());

		String destDirPath = destinationDir.getCanonicalPath();
		String destFilePath = destFile.getCanonicalPath();

		if (!destFilePath.startsWith(destDirPath + File.separator)) {
			throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
		}

		return destFile;
	}

	public void pdfToTextConverter(String inputFileName, String outputFileName, boolean summarizationFlag,String fileName) {
		outputFileName = outputFileName.replace("pdf", "txt");
		try {
			PDDocument document = PDDocument.load(new File(inputFileName));
			PDFTextStripper stripper = new PDFTextStripper();
			String text = stripper.getText(document);
			String summarizedText = chat(text);

			document.close();

			if (summarizationFlag) {
				try (Writer writer = new FileWriter(outputFileName)) {
					writer.write(summarizedText);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				try (Writer writer = new FileWriter(outputFileName)) {
					writer.write(text);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void excelToTextConverter(String inputFileName, String outputFileName, boolean summarizationFlag,String fileName) {
		outputFileName = outputFileName.replace("xlsx", "txt");
		try (FileInputStream fis = new FileInputStream(inputFileName);
				Workbook workbook = new XSSFWorkbook(fis);
				FileWriter writer = new FileWriter(outputFileName)) {
			Sheet sheet = workbook.getSheetAt(0);
			for (Row row : sheet) {
				for (Cell cell : row) {
					if (cell.getCellType() == CellType.STRING) {
						String text = cell.getStringCellValue();
						String summarizedText = chat(text);
						if (summarizationFlag) {
							writer.write(summarizedText + "\t");
						} else {
							writer.write(text + "\t");
						}

					} else if (cell.getCellType() == CellType.NUMERIC) {
						double value = cell.getNumericCellValue();
						writer.write(String.valueOf(value) + "\t");
					} else if (cell.getCellType() == CellType.BOOLEAN) {
						boolean value = cell.getBooleanCellValue();
						writer.write(String.valueOf(value) + "\t");
					} else {
						// Handle other cell types as needed
					}
				}
				writer.write("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void docxToTextConvert(String inputFileName, String outputFileName, boolean summarizationFlag,String fileName) {
		outputFileName = outputFileName.replace("docx", "txt");
		try (FileInputStream fis = new FileInputStream(inputFileName);
				XWPFDocument document = new XWPFDocument(fis);
				XWPFWordExtractor extractor = new XWPFWordExtractor(document);
				FileWriter writer = new FileWriter(outputFileName)) {
			String text = extractor.getText();
			String summarizedText = chat(text);
			if (summarizationFlag) {
				writer.write(summarizedText);
			} else {
				writer.write(text);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void pptxToTextConvert(String inputFileName, String outputFileName, boolean summarizationFlag,String fileName) {
		outputFileName = outputFileName.replace("pptx", "txt");
		try (FileInputStream fis = new FileInputStream(inputFileName);
				XMLSlideShow ppt = new XMLSlideShow(fis);
				XSLFExtractor extractor = new XSLFExtractor(ppt);
				FileWriter writer = new FileWriter(outputFileName)) {
			String text = extractor.getText();
			String summarizedText = chat(text);
			if (summarizationFlag) {
				writer.write(summarizedText);
			} else {
				writer.write(text);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getFileName(String fileName) {
		int dotIndex = fileName.lastIndexOf('.');

		if (dotIndex <= 0) {
			return fileName;
		} else {
			// Return the file name without the extension
			String nameWithoutExtension = fileName.substring(0, dotIndex);
			return (nameWithoutExtension + ".txt");
		}
	}

	public void removeFile(File file) {
		file.delete();
	}

	/**************************************************************************************************************/

	public String chat(String text) {
		String response = "";
		try {
			response = chatWithGpt3("get summarized text " + text);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return response;
	}

	@Value("${openai.api_key}")
	private String openaiApiKey;

	private final HttpClient client = HttpClient.newHttpClient();

	private static final String GPT_3_URL = "https://api.openai.com/v1/completions";

	public String postToOpenAiApi(String requestBodyAsJson) throws IOException, InterruptedException {
		var request = HttpRequest.newBuilder().uri(URI.create(GPT_3_URL))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + openaiApiKey)
				.POST(BodyPublishers.ofString(requestBodyAsJson)).build();
		return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
	}

	@Autowired
	private ObjectMapper jsonMapper;

	private String chatWithGpt3(String message) throws Exception {
		var completion = CompletionRequest.defaultWith(message);
		String postBodyJson = jsonMapper.writeValueAsString(completion);
		var responseBody = postToOpenAiApi(postBodyJson);
		CompletionResponse completionResponse = jsonMapper.readValue(responseBody, CompletionResponse.class);
		return (String) completionResponse.firstAnswer();
	}

}

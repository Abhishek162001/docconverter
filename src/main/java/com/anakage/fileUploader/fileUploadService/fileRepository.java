package com.anakage.fileUploader.fileUploadService;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface fileRepository extends CrudRepository<fileData,Long>{

	public List<fileData> findAll(); 
	public Optional<fileData> findById(Long id);
}

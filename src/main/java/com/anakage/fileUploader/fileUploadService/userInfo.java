package com.anakage.fileUploader.fileUploadService;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class userInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long userId;
	public String userName;
	public String hostName;
	public String serialNumber;
	
	public userInfo()
	{
		super();
	}
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public userInfo(String userName, String hostName, String serialNumber) {
		super();
		this.userName = userName;
		this.hostName = hostName;
		this.serialNumber = serialNumber;
	}

	public String getUserName() {
		// TODO Auto-generated method stub
		return this.userName;
	}

}

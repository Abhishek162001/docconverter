package com.anakage.fileUploader.fileUploadService;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;

@Entity
public class fileData {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long fileId;
	public String fileName;
	public String filePath;
	public String fileType;
	@Transient
	public Boolean flag;
	public fileData()
	{
		super();
	}
	public fileData(String fileName, String filePath,String fileType) {
		super();
		this.fileName = fileName;
		this.filePath = filePath;
		this.fileType= fileType;
	}
}

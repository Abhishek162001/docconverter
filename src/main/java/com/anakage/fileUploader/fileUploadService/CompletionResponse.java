package com.anakage.fileUploader.fileUploadService;

import java.util.List;

import com.anakage.fileUploader.fileUploadService.CompletionResponse.Choice;
import com.anakage.fileUploader.fileUploadService.CompletionResponse.Usage;
//import java.util.Optional;

public record CompletionResponse(Usage usage, List<Choice> choices) {

	public String firstAnswer() {
		if (choices == null || choices.isEmpty())
			return "no response";
		String response = choices.get(0).text;
		return response;
	}

	record Usage(int total_tokens, int prompt_tokens, int completion_tokens) {
	}

	record Choice(String text) {
	}
}
